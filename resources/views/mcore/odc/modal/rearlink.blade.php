<div id="rearlink-modal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>

      <div class="modal-body">
        <div class="exists-link-view hidden"></div>
        <div class="new-link-view">
          <div class="text-center">Input data sambungan ke</div>
          <div class="row">
            <div class="col-xs-5 col-md-4 col-md-push-2">
              <a id="btn-link-odf" class="btn btn-info width-full pull-right">
                <i class="fas fa-long-arrow-alt-left"></i>
                <span>STO/ODF</span>
              </a>
            </div>
            <div class="col-xs-5 col-md-4 col-md-push-2">
              <a id="btn-link-odp" class="btn btn-info width-full">
                <span>ODP</span>
                <i class="fas fa-long-arrow-alt-right"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
