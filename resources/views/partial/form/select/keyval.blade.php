@component(
  'partial.form.select._tpl',
  ['field' => $field, 'label' => ($label ?? null), 'object' => $object, 'canEdit' => $canEdit, 'attributes' => ($attributes ?? null)]
)
  @foreach($options as $key => $val)
    <?php $selected = (old($field) ?: ($object->$field ?? '')) == $key ? 'selected' : '' ?>
    <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
  @endforeach

  @slot('readonly')
    <?php $value = $object->$field ?? array_keys($options)[0] ?? '' ?>
    <?php $key = isset($object->$field) ? $options[$object->$field] : reset($options) ?>
    <input type="hidden" name="{{ $field }}" value="{{ $value }}">
    <p class="form-control-static">{{ $key }}</p>
  @endslot
@endcomponent
