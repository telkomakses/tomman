#!/usr/bin/env bash

DIR=`dirname "$(readlink -f "$0")"`/..

pushd $DIR/resources/assets/vendor/pixeladmin
yarn install
yarn run bower:install
gulp compile-js
popd

pushd $DIR

if [ ! -f .env ]; then
    cp .env.example .env
fi

php artisan key:generate

yarn install
yarn run dev
popd
