<script>
  window.route.map = (() => {
    const c = window.McoreMap.const;

    const $mapView = $('#map-view');
    const mapView = $mapView[0];

    const setMapHeight = () => {
      const offset = $mapView.offset().top;
      const footerHeight = $('.px-footer-bottom').outerHeight();
      const parentMargins = 40;
      const height = window.innerHeight - offset - footerHeight - parentMargins;
      $mapView.css({ height });
      $('#point-view').css({ height });
    };

    const init = () => {
      setMapHeight();

      window.map = new google.maps.Map(mapView, {
        center: c.center,
        zoom: c.defaultZoom,
        minZoom: c.defaultZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        clickableIcons: false,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        }
      });
    };

    return { init };
  })();
</script>
