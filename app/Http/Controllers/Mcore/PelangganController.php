<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitPelanggan;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\PelangganCached;
use App\Service\Mcore\Link\OdpToOntCached;

class PelangganController extends Controller
{
    public function index()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.pelanggan', Authorization::WRITE);
        [$workzoneTree, $wz_mtime] = PelangganCached::countByWorkzonePath($currentUser->workzone_path);

        $count = count($workzoneTree) + count($workzoneTree[0]->children);
        if ($count == 1) {
            return redirect('/mcore/pelanggan/workzone/'.$workzoneTree[0]->id);
        }

        return view('mcore.pelanggan.index', compact('canCreateNew', 'workzoneTree'));
    }

    public function listByWorkzone(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$pelangganList, $mtime] = PelangganCached::paginateByWorkzonePath($workzoneData->path, $page, $search);
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.pelanggan', Authorization::WRITE);

        return view('mcore.pelanggan.list', compact('workzoneData', 'pelangganList', 'canCreateNew'));
    }

    public function createForm()
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $canEdit = true;
        $types = PelangganCached::TYPES;

        return view('mcore.pelanggan.form', compact('workzoneTree', 'canEdit', 'types'));
    }

    public function create(SubmitPelanggan $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = PelangganCached::create(
                $user->id,
                $request->workzone_id,
                $request->kode,
                $request->label,
                $request->type,
                $request->pic,
                $request->alamat,
                $request->keterangan,
                $request->lat,
                $request->lng
            );
            $link = '<a href="/mcore/pelanggan/'.$id.'">'.$request->label.'</a>';

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data Pelanggan '.$link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function updateForm($id)
    {
        [$pelangganData, $mtime] = PelangganCached::getById($id);
        if (!$pelangganData) {
            abort(404);
        }

        [$link, $link_mtime] = OdpToOntCached::getByPelanggan($id);

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.pelanggan', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $pelangganData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        [$workzoneTree, $workzoneLastModified] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $types = PelangganCached::TYPES;

        return view(
            'mcore.pelanggan.form',
            compact('pelangganData', 'workzoneTree', 'canEdit', 'statuses', 'types', 'link')
        );
    }

    public function update(SubmitPelanggan $request, $id)
    {
        [$pelangganData, $mtime] = PelangganCached::getById($id);
        if (!$pelangganData) {
            abort(404);
        }

        try {
            $user = SessionHelper::getCurrentUser();
            PelangganCached::update(
                $user->id,
                $id,
                $request->workzone_id,
                $request->kode,
                $request->label,
                $request->type,
                $request->pic,
                $request->alamat,
                $request->keterangan,
                $request->lat,
                $request->lng
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data Pelanggan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
