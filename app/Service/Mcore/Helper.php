<?php

namespace App\Service\Mcore;

class Helper
{
    public const TYPE_STO = 'sto';
    public const TYPE_STO_ROOM = 'sto_room';
    public const TYPE_ODF = 'odf';
    public const TYPE_ODF_PANEL = 'odf_panel';
    public const TYPE_OTB = self::TYPE_ODF_PANEL;
    public const TYPE_ODF_OTB = self::TYPE_ODF_PANEL;
    public const TYPE_FEEDER = 'feeder';
    public const TYPE_ODC = 'odc';
    public const TYPE_ODC_SPLIITER = 'odc_splitter';
    public const TYPE_DISTRIBUSI = 'distribusi';
    public const TYPE_ODP = 'odp';
    public const TYPE_PELANGGAN = 'pelanggan';
    public const TYPE_ONT = self::TYPE_PELANGGAN;
    public const TYPE_ALPRO = 'alpro';

    public const CORE_PER_TUBE = 12;

    public const COLORS = [
        1 => 'Biru',
        'Orange',
        'Hijau',
        'Coklat',
        'Abu-abu',
        'Putih',
        'Merah',
        'Hitam',
        'Kuning',
        'Ungu',
        'Pink',
        'Tosca',
        'Biru-Arsir',
        'Orange-Arsir',
        'Hijau-Arsir',
        'Coklat-Arsir',
        'Abu-abu-Arsir',
        'Putih-Arsir',
        'Merah-Arsir',
        'Hitam-Arsir',
        'Kuning-Arsir',
        'Ungu-Arsir',
        'Pink-Arsir',
        'Tosca-Arsir'
    ];

    public static function getTubeCore($coreId)
    {
        $tubeNum = ceil($coreId / self::CORE_PER_TUBE);
        $coreNum = $coreId - (($tubeNum - 1) * self::CORE_PER_TUBE);

        return [$tubeNum, $coreNum];
    }

    public static function getCoreData($coreId)
    {
        $tubeCore = self::getTubeCore($coreId);
        $tubeNum = $tubeCore[0];
        $coreNum = $tubeCore[1];

        return compact('coreId', 'tubeNum', 'coreNum');
    }

    public static function odfPanelJoins(&$query)
    {
        $query
            ->join('mcore.odf', 'odf_panel.odf_id', '=', 'odf.id')
            ->join('mcore.sto_room', 'odf.sto_room_id', '=', 'sto_room.id')
            ->join('mcore.sto', 'sto_room.sto_id', '=', 'sto.id')
            ->join('auth.workzone AS wz_sto', 'sto.workzone_id', '=', 'wz_sto.id')
        ;

        $query->addSelect(
            'odf.id AS odf_id',
            'odf.label AS odf_label',
            'sto_room.id AS sto_room_id',
            'sto_room.label AS sto_room_label',
            'sto.id AS sto_id',
            'sto.label AS sto_label',
            'sto.workzone_id AS sto_workzone_id',
            'wz_sto.label AS sto_workzone_label',
            'wz_sto.path AS sto_workzone_path'
        );

        return $query;
    }

    public static function refreshRouteDataPoint($routeData)
    {
        $devices = [];
        foreach ($routeData as $point) {
            $devices[$point->deviceType][] = $point->id;
        }

        // WARNING: multiple db round-trip
        $freshData = [];
        foreach ($devices as $device => $ids) {
            $freshData[$device] = Map::getByIdList($device, $ids);
        }

        foreach ($routeData as $data) {
            $index = array_search($data->id, array_column($freshData[$data->deviceType], 'id'));
            $fresh = $freshData[$data->deviceType][$index];
            foreach ($fresh as $key => $value) {
                $data->$key = $value;
            }

            if ($data->deviceType == self::TYPE_ALPRO) {
                $data->typeAsText = Alpro::TYPES[$data->type];
            }
        }

        return $routeData;
    }
}
