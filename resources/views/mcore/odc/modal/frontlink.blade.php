<div id="frontlink-modal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <form id="form-splitter" class="modal-content" action="/mcore/odc/{{ $odcData->id }}/splitter" method="post">
      {{ csrf_field() }}
      <input id="input-odc-panel" name="odc_panel" type="hidden">
      <input id="input-odc-port" name="odc_port" type="hidden">

      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>

      <div class="modal-body">
        <fieldset class="form-group form-message-light">
          <label>
            <span id="label-splitter-new" class="hidden">Label/Nama/Kode</span>
            <span>Splitter</span>
          </label>
          <div class="row">
            <div class="col-md-9">
              <input id="input-splitter-label" name="splitter_label" class="form-control hidden" autocomplete="off"
                     data-msg-required="Silahkan isi data ini">
              <div id="container-splitter">
                <select id="input-splitter" name="splitter_id" class="form-control"
                        data-msg-required="Silahkan isi data ini">
                </select>
              </div>
              <p id="readonly-splitter-label" class="form-control-static hidden"></p>
            </div>
            <div class="col-md-3">
              <label id="check-splitter-new" class="custom-control custom-checkbox m-xs-t-10 m-md-t-5">
                <input id="input-splitter_new" name="splitter_new" class="custom-control-input" type="checkbox">
                <span class="custom-control-indicator"></span>
                <span>Splitter Baru</span>
              </label>
              <button id="btn-frontlink-unplug" name="unplug" value="1" class="btn btn-danger pull-right hidden">
                <i class="fas fa-unlink"></i>
                <span>Cabut</span>
              </button>
            </div>
          </div>
          <small id="text-splitter-descriptor" class="text-muted">
            Pilih Splitter yang sudah ada atau input Splitter Baru
          </small>
        </fieldset>

        <fieldset class="form-group form-message-light">
          <label>Port Splitter</label>
          <div id="container-splitter-port">
            <select name="splitter_port" id="input-splitter-port" class="form-control"
                    required data-msg-required="Silahkan isi data ini">
            </select>
          </div>
          <i id="port-loading" class="fas fa-spinner fa-pulse hidden"></i>
          <p id="readonly-splitter-port" class="form-control-static hidden"></p>
        </fieldset>
      </div>

      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="reset" data-dismiss="modal">
          <i class="fas fa-ban"></i>
          <span>Batal</span>
        </button>

        <button id="btn-frontlink-save" class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    </form>
  </div>
</div>
