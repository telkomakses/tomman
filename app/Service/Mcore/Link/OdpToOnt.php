<?php

namespace App\Service\Mcore\Link;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\PelangganCached;
use App\Service\Mcore\OdpCached;

class OdpToOnt
{
    public const SRC_TYPE = 'odp';
    public const DST_TYPE = 'ont';

    private static function table()
    {
        return DB::table('mcore.link');
    }

    private static function db()
    {
        return self::table()
            ->join('mcore.odp', function ($join) {
                $join->on('link.src_type', DB::raw("'".self::SRC_TYPE."'"))
                     ->on('link.src_id', 'odp.id');
            })
            ->join('auth.workzone AS wz_odp', 'odp.workzone_id', '=', 'wz_odp.id')

            ->join('mcore.pelanggan', function ($join) {
                $join->on('link.dst_type', DB::raw("'".self::DST_TYPE."'"))
                     ->on('link.dst_id', 'pelanggan.id');
            })
            ->join('auth.workzone AS wz_pelanggan', 'pelanggan.workzone_id', '=', 'wz_pelanggan.id')

            ->select(
                'link.*',
                //
                'odp.id AS odp_id',
                'odp.label AS odp_label',
                'odp.workzone_id AS odp_workzone_id',
                'wz_odp.label AS odp_workzone_label',
                //
                'pelanggan.id AS pelanggan_id',
                'pelanggan.kode AS pelanggan_kode',
                'pelanggan.label AS pelanggan_label',
                'pelanggan.workzone_id AS pelanggan_workzone_id',
                'wz_pelanggan.label AS pelanggan_workzone_label'
            )
        ;
    }

    /**
     * @param int $user_id for history/audit
     * @param int $odpId
     * @param int $odpPort
     * @param string $mediumLabel
     * @param int $pelangganId
     * @throws \Throwable when database transaction failed
     */
    public static function save(int $user_id, int $odpId, int $odpPort, string $mediumLabel, int $pelangganId)
    {
        $data = [
            'src_type' => self::SRC_TYPE,
            'src_id' => $odpId,
            'src_val' => $odpPort,
            'dst_type' => self::DST_TYPE,
            'dst_id' => $pelangganId,
            'med_val' => $mediumLabel
        ];

        DB::transaction(function () use ($data, $user_id) {
            $criteria = self::getKeyCriteria($data);
            $affectedRows = self::table()->where($criteria)->update(['med_val' => $data['med_val']]);
            if ($affectedRows < 1) {
                self::table()->insert($data);
            }

            OdpCached::insertHistory($user_id, $data['src_id'], 'link.plug', $data);
            PelangganCached::insertHistory($user_id, $data['dst_id'], 'link.plug', $data);
        });
    }

    /**
     * @param int $user_id for history/audit
     * @param int $odpId
     * @param int $odpPort
     * @param int $pelangganId
     * @throws \Throwable when database transaction failed
     */
    public static function remove(int $user_id, int $odpId, int $odpPort, int $pelangganId)
    {
        $data = [
            'src_type' => self::SRC_TYPE,
            'src_id' => $odpId,
            'src_val' => $odpPort,
            'dst_type' => self::DST_TYPE,
            'dst_id' => $pelangganId
        ];

        DB::transaction(function () use ($data, $user_id) {
            $condition = self::getKeyCriteria($data);
            self::table()->where($condition)->delete();

            OdpCached::insertHistory($user_id, $data['src_id'], 'link.unplug', $data);
            PelangganCached::insertHistory($user_id, $data['dst_id'], 'link.unplug', $data);
        });
    }

    private static function getKeyCriteria($data)
    {
        unset($data['med_val']);

        $condition = [];
        foreach ($data as $key => $val) {
            $condition[] = [$key, '=', $val];
        }

        return $condition;
    }

    /**
     * @param $odpId
     * @param bool $odpPort
     * @return mixed all plugged links for ODP, sorted by port number (src_val)
     */
    public static function getByOdp($odpId, $odpPort = false)
    {
        $query = self::db()
            ->where('src_type', self::SRC_TYPE)
            ->where('dst_type', self::DST_TYPE)
            ->where('src_id', $odpId)
            ->orderBy('src_val')
        ;

        if ($odpPort) {
            $query->where('src_val', $odpPort);
            return $query->first();
        } else {
            return $query->get();
        }
    }

    public static function getByPelanggan($pelangganId)
    {
        return self::db()
            ->where('src_type', self::SRC_TYPE)
            ->where('dst_type', self::DST_TYPE)
            ->where('dst_id', $pelangganId)
            ->first()
        ;
    }

    public static function paginatePelangganByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $query = DB::table('mcore.pelanggan AS pelanggan')
            ->leftJoin('auth.workzone AS workzone', 'pelanggan.workzone_id', '=', 'workzone.id')
            ->leftJoin('mcore.link AS link', function ($join) {
                $join->on('pelanggan.id', 'link.dst_id')
                     ->on('link.src_type', DB::raw("'".self::SRC_TYPE."'"))
                     ->on('link.dst_type', DB::raw("'".self::DST_TYPE."'"));
            })
            ->leftJoin('mcore.odp AS odp', 'link.src_id', '=', 'odp.id')
            ->select(
                'pelanggan.id',
                'pelanggan.label',
                'pelanggan.kode',
                'pelanggan.type',
                //
                'odp.id AS odp_id',
                'odp.label AS odp_label',
                'link.src_val AS odp_port',
                //
                'med_val AS link_medium'
            )
            ->where('workzone.path', '<@', $path)
        ;

        if ($search) {
            $clause = '(pelanggan.label ILIKE ? OR kode ILIKE ? OR pic ILIKE ?)';
            $term = '%'.str_replace(' ', '%', $search).'%';
            $param = array_fill(0, 3, "%$term%");

            $query->whereRaw($clause, $param);
        }

        return $query->paginate($limit, ['*'], 'NOTUSED', $page);
    }
}
