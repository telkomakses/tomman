@extends('app')

@section('title', 'Pelanggan')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>Pelanggan per WorkZone</span>
    </h1>

    @if ($canCreateNew)
      <a href="/mcore/pelanggan/new" class="btn btn-info pull-right">
        <i class="fas fa-plus"></i>
        <span>Input Pelanggan</span>
      </a>
    @endif
  </div>

  <ul class="tree tree-links">
    @each('partial.treelink', $workzoneTree, 'tree')
  </ul>
@endsection
