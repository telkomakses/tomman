<script>
  $(() => {
    const $ports = $('#ports');
    const $modal = $('#port-modal');
    const $modalTitle = $modal.find('.modal-title');
    const $existsLinkView = $modal.find('.exists-link-view');
    const $newLinkView = $modal.find('.new-link-view');
    const $btnLinkToOlt = $('#btn-link-olt');
    const $btnLinkToOdc = $('#btn-link-odc');

    const hiddenClass = 'hidden';

    let $currentPortHidden;
    let $currentPortLinkView;

    $ports.on('click', '.rearport', event => {
      const $target = $(event.currentTarget);
      const port = $target.data('port');

      $modalTitle.text('Port '+port);

      if ($target.hasClass('up')) {
        $currentPortHidden = $target.children('.hidden');
        $currentPortLinkView = $currentPortHidden.children('.link-view').detach();

        $existsLinkView.removeClass(hiddenClass).append($currentPortLinkView);
        $newLinkView.addClass(hiddenClass);
      } else {
        $currentPortHidden = $currentPortLinkView = null;

        $existsLinkView.addClass(hiddenClass);
        $newLinkView.removeClass(hiddenClass);

        const url = '/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/{{ $odfData->id }}/panel/{{ $panelData->id ?? 0 }}';
        $btnLinkToOdc.attr('href', `${url}/port/${port}/odc`);
        $btnLinkToOlt.attr('href', `${url}/port/${port}/olt`);
      }

      $modal.modal('show');
    });

    $modal.on('hidden.bs.modal', event => {
      if (!$currentPortHidden) return;

      $currentPortLinkView.detach().appendTo($currentPortHidden);
    });
  });
</script>
