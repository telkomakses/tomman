<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdfPanelAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odf_panel_audit(
              id BIGSERIAL PRIMARY KEY,
              odf_panel_id BIGINT REFERENCES mcore.odf_panel(id),
              user_id INTEGER REFERENCES auth.user(id),
              operation TEXT NOT NULL CHECK (operation <> ''),
              timestamp TIMESTAMP WITH TIME ZONE, 
              data JSON NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odf_panel_audit(odf_panel_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odf_panel_audit');
    }
}
