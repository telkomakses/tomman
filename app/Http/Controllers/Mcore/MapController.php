<?php

namespace App\Http\Controllers\Mcore;

use App\Http\Controllers\Controller;
use App\Service\Mcore\Map;
use App\Service\Mcore\Helper as Mcore;

class MapController extends Controller
{
    public function mapView()
    {
        return view('mcore.map.view');
    }

    public function allSto()
    {
        return Map::getAll(Mcore::TYPE_STO);
    }

    public function grid($device, $zoom, $x, $y)
    {
        return Map::getByGrid($device, $zoom, $x, $y);
    }

    public function cellTest($zoom, $x, $y)
    {
        return Map::cellTest($zoom, $x, $y);
    }
}
