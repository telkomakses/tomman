<script>
  $('#{{ $id }}').pxValidate({
    submitHandler: form => {
      form.className += ' form-loading form-loading-inverted';
      form.submit();
    }
  });
</script>
