<?php

namespace App\Service;

class CoordinateHelper
{
    public static function parseLatLngFromText($text)
    {
        if (!$text) {
            return [
                'lat' => 0,
                'lng' => 0
            ];
        }

        $token = explode(',', $text);
        $lat = floatval(trim($token[0]) ?: 0);
        $lng = floatval(trim($token[1]) ?: 0);

        return compact('lat', 'lng');
    }
}
