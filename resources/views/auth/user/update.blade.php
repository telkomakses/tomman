@extends('app')

@section('title', 'User '.$data->login)

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-user-edit"></i>
      <span>
        @if ($data->type == \App\Service\Auth\User::TYPE_NEWUSER)
          Aktivasi
        @elseif ($canUpdate)
          Edit Data
        @endif

        User {{ $data->login }}
      </span>
    </h1>
  </div>

  <div class="row">
    <div class="col-md-6 col-md-push-3">
      <form id="formUserUpdate" class="panel form-horizontal" method="post">
        {{ csrf_field() }}
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">
              @if ($data->type == App\Service\Auth\User::TYPE_LOCALUSER)
                Login
              @else
                NIK
              @endif
            </label>
            <div class="col-sm-4">
              <p class="form-control-static">
                @if ($data->type == App\Service\Auth\User::TYPE_LOCALUSER)
                  <span class="label label-info">local</span>
                @else
                  <span class="label label-primary">SSO</span>
                @endif

                {{ $data->login }}
              </p>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-4">
              <p class="form-control-static">{{ $data->nama }}</p>
            </div>
          </div>

          {{--<div class="form-group">
            <label class="col-sm-3 control-label">Company</label>
            <div class="col-sm-6">
              <select class="form-control custom-select">
                <option>Telkom Akses</option>
                <option>Mitra</option>
                <option>TELKOM</option>
              </select>
            </div>
          </div>--}}

          <?php $hasError = $errors->has('timezone') ?>
          <div class="form-group form-message-light">
            <label for="input-timezone" class="col-sm-3 control-label">Timezone</label>
            <div class="col-sm-6">
              <select name="timezone" class="custom-select form-control" required data-msg-required="Silahkan pilih zona waktu">
                @foreach($timezoneList as $key => $val)
                  <option value="{{ $key }}" {{ (($data->timezone ?? 0) == $key) ? 'selected' : '' }}>{{ $val[0] }}</option>
                @endforeach
              </select>
              @if ($hasError)
                @foreach($errors->get('timezone') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </div>

          <div class="form-group form-message-light">
            <label class="col-sm-3 control-label">Work Zone</label>
            <div class="col-sm-6">
              @if ($canUpdate)
                <div class="input-group">
                  <input id="input-workzone_id" name="workzone_id" type="hidden" data-msg-required="Silahkan pilih WorkZone" value="{{ $data->workzone_id }}" required>
                  <input id="input-workzone_text" class="form-control" value="{{ $data->workzone_label }}" readonly>
                  <span class="input-group-btn">
                    <button id="btnPick-workzone_id" class="btn btn-default" type="button">Select</button>
                  </span>
                </div>
              @else
                <p class="form-control-static">{{ $data->workzone_label }}</p>
              @endif
            </div>
          </div>

          <div class="form-group form-message-light">
            <label class="col-sm-3 control-label">Permission</label>
            <div class="col-sm-6">
              @if ($canUpdate)
                <select class="form-control" name="permission_id" data-msg-required="Silahkan pilih Permission" required>
                  <option></option>
                  @foreach ($permissionList as $entry)
                    <option value="{{ $entry->id }}" {{ $entry->id == $data->permission_id ? 'selected' : '' }}>{{ $entry->title }}</option>
                  @endforeach
                </select>
              @else
                <p class="form-control-static">{{ $data->permission_title }}</p>
              @endif
            </div>
          </div>
        </div>

        @if ($canUpdate)
          <div class="panel-footer text-right">
            <button class="btn btn-primary">
              <i class="fas fa-check"></i>
              <span>Simpan</span>
            </button>
          </div>
        @endif
      </form>
    </div>
  </div>
@endsection

@section('script')
  @if ($canUpdate)
    @include('partial.workzone.modal')
    @include('partial.workzone.script', [
      'field' => 'workzone_id',
      'displayField' => 'workzone_text',
      'workzoneTree' => $workzoneTree,
      'canEdit' => $canUpdate
    ])

    <script>
      $(function() {
        $('select[name=permission_id]').select2({
          placeholder: '...'
        });
      });
    </script>

    @include('partial.form.validate', ['id' => 'formUserUpdate'])
  @endif
@endsection
