<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Auth\Workzone;

class Feeder
{
    public const CAPACITIES = [48, 96, 144, 288];

    public const ERR_PARENT_NOT_FOUND = 404;

    private static function table()
    {
        return DB::table('mcore.feeder');
    }

    private static function db()
    {
        return self::table()
            ->leftJoin('auth.workzone', 'feeder.workzone_id', '=', 'workzone.id')
            ->select(
                'feeder.id',
                'feeder.label',
                'feeder.workzone_id',
                'capacity',
                //
                'workzone.label AS workzone_label',
                'workzone.path AS workzone_path'
            )
            ->orderBy('feeder.label')
        ;
    }

    private static function auditTable()
    {
        return DB::table('mcore.feeder_audit');
    }

    public static function countByWorkzonePath(string $path)
    {
        $sql = "
            SELECT
              id,
              label,
              path,
              '/mcore/feeder/workzone/' || id AS url,
              CASE 
                WHEN feeder.count IS NOT NULL THEN feeder.count
                ELSE 0
              END AS count
              
            FROM
              auth.workzone
              
            LEFT JOIN
              (SELECT workzone_id, COUNT(workzone_id) FROM mcore.feeder GROUP BY workzone_id) AS feeder
              ON feeder.workzone_id = id
              
            WHERE
              path <@ ?
              
            ORDER BY
              path
        ";
        $params = [$path];
        $result = DB::select($sql, $params);

        return Workzone::grow($result);
    }

    /**
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param int $capacity
     * @param int|null $parent_id
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(int $user_id, int $workzone_id, string $label, int $capacity, int $parent_id = null)
    {
        $id = 0;
        $data = compact('workzone_id', 'label', 'capacity');

        DB::transaction(function () use (&$id, $user_id, $data, $parent_id) {
            $id = self::table()->insertGetId($data);

            if ($parent_id) {
                $parent = self::table()->select('branch_path')->where('id', $parent_id)->first();
                if (!$parent) {
                    throw new \Exception('Parent Not Found', self::ERR_PARENT_NOT_FOUND);
                }

                $branch_path = $parent->branch_path.'.'.$id;
            } else {
                $branch_path = $id;
            }
            self::table()->where('id', $id)->update(compact('branch_path'));

            self::insertHistory($user_id, $id, 'insert', $data);
        });

        return $id;
    }

    public static function insertHistory($user_id, $feeder_id, $operation, array $data)
    {
        $timestamp = DB::raw("NOW() AT TIME ZONE 'utc'");
        $data = json_encode($data);
        $historyData = compact('feeder_id', 'user_id', 'timestamp', 'operation', 'data');

        self::auditTable()->insert($historyData);
    }

    public static function getById($id)
    {
        return self::db()->where('feeder.id', $id)->first();
    }

    /**
     * @param int $user_id for history/audit
     * @param int $feeder_id
     * @param int $workzone_id
     * @param string $label
     * @param int $capacity
     * @throws \Throwable when database transaction failed
     */
    public static function update(int $user_id, int $feeder_id, int $workzone_id, string $label, int $capacity)
    {
        $data = compact('workzone_id', 'label', 'capacity');

        DB::transaction(function () use ($feeder_id, $data, $user_id) {
            self::table()->where('id', $feeder_id)->update($data);

            self::insertHistory($user_id, $feeder_id, 'update', $data);
        });
    }

    /**
     * @param int $user_id for history/audit
     * @param int $feeder_id
     * @param string $route_path in JSON format
     * @throws \Throwable when database transaction failed
     */
    public static function saveRoutePath(int $user_id, int $feeder_id, string $route_path)
    {
        DB::transaction(function () use ($user_id, $feeder_id, $route_path) {
            $data = compact('route_path');
            self::table()->where('id', $feeder_id)->update($data);

            self::insertHistory($user_id, $feeder_id, 'update:route_path', $data);
        });
    }

    public static function getRoutePath($feeder_id)
    {
        $data = self::table()->select('route_path')->where('id', $feeder_id)->first();
        return json_decode($data->route_path);
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $query = self::db()->where('workzone.path', '<@', $path);

        if ($search) {
            $clause = 'feeder.label ILIKE ?';
            $term = '%'.str_replace(' ', '%', $search).'%';

            $query->whereRaw($clause, ["%$term%"]);
        }

        return $query->paginate($limit, ['*'], 'NOTUSED', $page);
    }

    public static function linksToCores(int $capacity, array $linkList)
    {
        $corePerTube = 12;
        $tubeCount = $capacity / $corePerTube;

        $result = [];
        for ($i = 1; $i <= $tubeCount; $i++) {
            $coreInTube = (object)[
                'tubeNum' => $i,
                'coreList' => []
            ];
            for ($j = 1; $j <= $corePerTube; $j++) {
                $currentLink = null;
                $linkFound = null;
                $coreId = $j + (($i - 1) * $corePerTube);

                foreach ($linkList as $linkIndex => $link) {
                    if ($link->med_val == $coreId) {
                        $currentLink = $link;
                        $linkFound = $linkIndex;
                        break;
                    }
                }

                $coreInTube->coreList[] = (object)[
                    'coreId' => $coreId,
                    'coreNum' => $j,
                    'link' => $currentLink ?? false
                ];

                if ($linkFound !== null) {
                    array_splice($linkList, $linkFound, 1);
                }
            }
            $result[] = $coreInTube;
        }

        return $result;
    }
}
