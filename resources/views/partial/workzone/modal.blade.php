<div id="workzoneModal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <h4 id="workzoneModal_title" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <ul class="tree">
          @each('partial.tree', $workzoneTree, 'tree')
        </ul>
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning pull-left" data-dismiss="modal" type="reset">
          <i class="fas fa-ban"></i>
          <span>Batal</span>
        </button>
      </div>
    </div>
  </div>
</div>

<script>
  window.WorkzoneModal = (() => {
    const $modal = $('#workzoneModal');
    const $title = $('#workzoneModal_title');

    let callback;
    let canEdit = true;
    let $listItems = null;
    let $selectedItem = null;

    const setSelection = id => {
      if (!$listItems) $listItems = $modal.find('.tree-selectable');
      if ($selectedItem) $selectedItem.removeClass('selected');

      id = Number(id);

      for (let i = 0; i < $listItems.length; i++) {
        const $item = $($listItems.get(i));
        const itemId = Number($item.data('id'));
        if (itemId === id) {
          $selectedItem = $item;
          $item.addClass('selected');
          return;
        }
      }
    };

    $modal.on('click', '.tree-selectable', event => {
      if (!canEdit) return;

      $modal.modal('hide');

      if (!callback) {
        throw new Error("WorkzoneModal.setCallback() hasn't been called");
      }

      const $el = $(event.target);
      const id = $el.data('id');
      const nama = $el.data('nama');

      callback(id, nama);
    });

    return {
      setCallback: cb => callback = cb,
      setTitle: t => $title.text(t),
      setCanEdit: can => canEdit = can,

      open: selectedId => {
        if (selectedId) setSelection(selectedId);

        $modal.modal('show');
      }
    };
  })();
</script>
