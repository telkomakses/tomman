<?php

namespace App\Http\Requests\Mcore;

use Illuminate\Foundation\Http\FormRequest;
use App\Service\Auth\Authorization;
use App\Service\SessionHelper;

class SubmitOdfPanel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.sto.odf.otb', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';
        $required_numeric = $required.'|numeric';

        return [
            'label' => $required,
            'slotcount' => $required_numeric,
            'slotportcount' => $required_numeric,
            'slotvertical' => $required.'|boolean'
        ];
    }
}
