<?php

namespace App\Http\Requests\Mcore\Link;

use Illuminate\Foundation\Http\FormRequest;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;

class SubmitOdfOdc extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.link.otb-odc', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'src_id' => $required,
            'src_val' => $required,
            'med_id' => $required,
            'med_val' => $required,
            'dst_id' => $required,
            'dst_val' => $required,

            'action' => $required.'|in:plug,unplug'
        ];
    }
}
