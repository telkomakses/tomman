#!/usr/bin/env bash

DIR=`dirname "$(readlink -f "$0")"`
DIR=`readlink -f "$DIR/../"`

sed $DIR/supervisor.conf \
    -e "s|\$APP_PATH\/|$DIR/|" \
    | sudo tee /etc/supervisor/conf.d/tomman-worker.conf > /dev/null

sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start tomman-worker:*
