<?php

namespace App\Service\Mcore;

use App\Service\Cache;

class OdfCached
{
    const PREFIX = 'Mcore.Sto.Room.Odf:';
    const KEY_BY_ROOM = self::PREFIX.'StoRoom=';
    const KEY_BY_ID = self::PREFIX.'Id=';

    public static function keyByStoRoom($room_id)
    {
        return self::KEY_BY_ROOM.$room_id;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return Odf::getById($id);
        };

        return Cache::store($key, $dataSource);
    }

    public static function listByRoomId($roomId)
    {
        $key = self::keyByStoRoom($roomId);

        $dataSource = function () use ($roomId) {
            return Odf::listByRoomId($roomId);
        };

        return Cache::store($key, $dataSource);
    }

    /**
     * @param $userId
     * @param $roomId
     * @param $odfId
     * @param $label
     * @param $group
     * @throws \Throwable
     */
    public static function update($userId, $roomId, $odfId, $label, $group)
    {
        Odf::update($userId, $roomId, $odfId, $label, $group);

        Cache::flushKeys([self::keyByStoRoom($roomId), self::keyById($odfId)]);
    }

    /**
     * @param $userId
     * @param $roomId
     * @param $label
     * @param $group
     * @return int
     * @throws \Throwable
     */
    public static function create($userId, $roomId, $label, $group)
    {
        $id = Odf::create($userId, $roomId, $label, $group);

        Cache::del(self::keyByStoRoom($roomId));

        return $id;
    }

    public static function insertHistory($userId, $id, $operation, array $data)
    {
        Odf::insertHistory($userId, $id, $operation, $data);

        Cache::del(self::keyById($id));
    }
}
