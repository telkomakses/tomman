<?php

namespace App\Service\Mcore;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;

class FeederCached
{
    public const CAPACITIES = Feeder::CAPACITIES;
    public const ERR_PARENT_NOT_FOUND = Feeder::ERR_PARENT_NOT_FOUND;

    const PREFIX = 'Mcore.Feeder:';
    const KEY_COUNT = self::PREFIX.'Count;WorkzonePath=';
    const KEY_BY_WORKZONE = self::PREFIX.'WorkzonePath=%s;Page=%s;Search=%s;Limit=%s';
    const KEY_BY_ID = self::PREFIX.'Id=';

    const TAG_LISTING = self::PREFIX.'List';

    public static function tagById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function tagByWorkzoneId($workzoneId)
    {
        return WorkzoneCached::tagById($workzoneId).';'.self::TAG_LISTING;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    /**
     * Tags: tagByWorkzoneId
     *
     * @param $path
     * @return array [$data, $lastModified]
     */
    public static function countByWorkzonePath($path)
    {
        $key = self::KEY_COUNT.$path;

        $dataSource = function () use ($path) {
            return Feeder::countByWorkzonePath($path);
        };

        $tagGenerator = function () use ($path) {
            $id = WorkzoneCached::idByPath($path);
            $tag = self::tagByWorkzoneId($id);

            return [$tag];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::flushTagByWorkzoneId
     *
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param int $capacity
     * @param int|null $parent_id
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(int $user_id, int $workzone_id, string $label, int $capacity, int $parent_id = null)
    {
        $id = Feeder::create($user_id, $workzone_id, $label, $capacity, $parent_id);

        WorkzoneCached::flushTagByWorkzoneId($workzone_id, [self::class, 'tagByWorkzoneId']);

        return $id;
    }

    public static function insertHistory($user_id, $feeder_id, $operation, array $data)
    {
        Feeder::insertHistory($user_id, $feeder_id, $operation, $data);

        Cache::del(self::keyById($feeder_id));
    }

    /**
     * Tags: tagById
     *
     * @param $id
     * @return array [$data, $lastModified]
     */
    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return Feeder::getById($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * flush: tagById, tagByWorkzoneId
     *
     * @param int $user_id for history/audit
     * @param int $feeder_id
     * @param int $workzone_id
     * @param string $label
     * @param int $capacity
     * @throws \Throwable when database transaction failed
     */
    public static function update(int $user_id, int $feeder_id, int $workzone_id, string $label, int $capacity)
    {
        Feeder::update($user_id, $feeder_id, $workzone_id, $label, $capacity);

        $key = self::keyById($feeder_id);
        WorkzoneCached::flushIfKeyExists(
            $key,
            $workzone_id,
            'workzone_id',
            'workzone_path',
            [self::class, 'tagByWorkzoneId'],
            [self::tagById($feeder_id)]
        );
    }

    /**
     * not cached
     *
     * @param int $user_id for history/audit
     * @param int $feeder_id
     * @param string $route_path in JSON format
     * @throws \Throwable when database transaction failed
     */
    public static function saveRoutePath(int $user_id, int $feeder_id, string $route_path)
    {
        Feeder::saveRoutePath($user_id, $feeder_id, $route_path);
    }

    /**
     * not cached
     *
     * @param $feeder_id
     * @return mixed
     */
    public static function getRoutePath($feeder_id)
    {
        return Feeder::getRoutePath($feeder_id);
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_BY_WORKZONE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return Feeder::paginateByWorkzonePath($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [self::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }

    /**
     * Not Cached
     *
     * @param int $capacity
     * @param array $linkList
     * @return array
     */
    public static function linksToCores(int $capacity, array $linkList)
    {
        return Feeder::linksToCores($capacity, $linkList);
    }
}
