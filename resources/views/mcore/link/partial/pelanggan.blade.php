<?php $className = isset($pelangganData->odp_id) ? 'default' : 'success' ?>

<span class="label label-{{ $className }}">{{ $pelangganData->kode }}</span>
<span>{{ $pelangganData->label }}</span>

@isset($pelangganData->odp_id)
  <i class="fas fa-long-arrow-alt-left text-info"></i>

  <span class="label label-outline label-primary">ODP</span>
  <span>{{ $pelangganData->odp_label }}</span>

  <span class="label label-outline label-default">PORT</span>
  <span>{{ $pelangganData->odp_port }}</span>
@endisset
