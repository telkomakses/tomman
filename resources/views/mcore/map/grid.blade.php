<script>
  window.McoreMap.grid = (() => {
    const c = window.McoreMap.const;
    const center = c.center;
    const grids = c.grids;

    const findGridByZoom = zoom => grids.find(grid => zoom >= grid.zoom);

    const calcCellAxis = (value, refPoint, size) => Math.floor((value - refPoint) / size);

    const calcGridCell = (zoom, lat, lng) => {
      const grid = findGridByZoom(zoom);
      if (!grid) return undefined;

      const x = calcCellAxis(lng, center.lng, grid.size.width);
      const y = calcCellAxis(lat, center.lat, grid.size.height);

      return {
        x,
        y,
        load: grid.load,
        visible: grid.visible || []
      };
    };

    return {
      calcGridCell
    }
  })();
</script>
