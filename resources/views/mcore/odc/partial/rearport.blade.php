@if ($portData->rear)
  <span>panel</span>
  <span class="label label-outline">{{ $portData->panel }}</span>

  <span>port</span>
  <span class="label label-outline">{{ $portData->port }}</span>

  @if ($portData->rear->dst_type == \App\Service\Mcore\Helper::TYPE_ODP)
    <i class="fas fa-long-arrow-alt-right text-success"></i>

    <span class="label label-primary">ODP</span>
    <span>{{ $portData->rear->odp_label }}</span>
  @elseif ($portData->rear->src_type == \App\Service\Mcore\Helper::TYPE_OTB)
    <i class="fas fa-long-arrow-alt-left text-success"></i>

    <span class="label label-primary">ODF</span>
    <span>{{ $portData->rear->odf_label }}</span>
  @endif
@else
  <span>panel</span>
  <span class="label label-outline label-info">{{ $portData->panel }}</span>

  <span>port</span>
  <span class="label label-outline label-info">{{ $portData->port }}</span>
@endisset
